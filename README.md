CivicWise Network Map
==================================================


**Work in progress**  
This map contains the members of the [CivicWise](https://civicwise.org) network.
Inspired by and based on [Cadáveres inmobiliarios](http://cadaveresinmobiliarios.org/mapa).
The file `index_OLD.html` is used for testing/experimenting, and currently uses the files `data/test.json`, `data/test.geojson`, and `js/leaflet-tag-filter-button.js` for testing the implementation of the tags/skills filter (which is **not working yet!**)

## Some preliminary considerations
Initially the map was to be created with [carto.com](https://carto.com/); however a series of issues arose, among which:

- the impossibility of clustering overlapping nodes (**crucial** as for *CivicFactories* there are multiple nodes/members on the same coordinates).
- a non-fully-cooperative platform: the completely-shared model was difficult to pursue as carto.com requires special permissions for cooperative editing, and some options are only available to paid accounts.
- creating a CivicWise shared account was considered, however this would have meant that all the contributions by other members would have been "lost". Git on the other hand allows to track down *who has done what* and to correctly give credit to each single "piece of help" that is given by other members.

Git certainly introduces a level of complexity that cannot be overlooked, but given the benefits outlined above, it is also more in line with the "nature" of the project (i.e. a *digital tool*).
For the time being Daniele and I are working on **two separate maps** (Daniele is working with carto.com, I am working on this map in [leaflet.js](https://leafletjs.com/)), so that we can evaluate pros and cons of the two approaches and the decide all together if the increased complexity is worth.

## General info
Starting from the map created by [Montera34](https://montera34.com/), the index.html was modified to accept the .geojson containing the answers from the Google Form.
In order to create the .geojson I followed the steps below:

- added the two columns `lat` and `lon` for latitude and longitude coordinates. I then manually converted the addresses into coordinates using [gps-coordinates](https://www.gps-coordinates.net/). I did not do it automatically because there is no standard in the format people have used to indicate the address, and using a geocoder (such as the one in Python) would result in several false positives. See the *Improve the Google Form to "force" a standard for certain answers* point in the *TODO* section below.
- imported the Google spreadsheet to [carto.com](https://carto.com/)
- exported the [carto.com](https://carto.com/) dataset to .geojson

~~For the future, I think we'll need to create a script that automatically converts the spreasheet to .geojson, maybe using the [csv2geojson](https://github.com/mapbox/csv2geojson) script - *suggestions are welcome for other tools*.~~  
The process can now be automated using the `gform2geojson` script included in the `script/` folder.

##  *gform2geojson* script to convert the Google Form output to interactive map
"Version 1" of the script is now available in the folder `script`. It currently works as described below.

1. Modify the `Manual environment` configuration section in the script: `g_form_file` is the name of the input `.csv` file; `geojson_name` is the output filename; `g_api_key_file` is the file containing the Google API key (see included `google_api.txt` example).
2. The `.csv` file input (which must be placed in the same folder as the script), has to be created from the Google Form spreadsheet with the following options:  
`TAB` (`\t`) as *delimiter*  
`pipe character` (`|`) as *separator*  
This "non-standard" format was adopted because the csv fields contain single and double-quotes (`'` and `"`), commas (`,`), semicolons (`;`), single spaces (`\s`) and other characters that are commonly used as *delimiters*/*separators*.
3. Columns must be arranged in the following order (the one that is currently available in the GForm output):

  |Column letter|Name of column|
  |---|---|
  |A|"Informazioni cronologiche" (date and time of when the questionnaire was filled in)|
  |B|"Your name"|
  |C|"Your surname"|
  |D|"Where are you from (country)"|
  |E|"Where are you based (address)"|
  |F|"Describe yourself in one tweet"|
  |G|"Your skills and competences"|
  |H|"Language skills"|
  |I|"Social links"|
  |J|"Profile picture"|

4. The script requires a valid Google API key (see point 1) to download the profile pictures from the Drive folder. The images are downloaded to the folder `images/` - it **overwrites** previously existing images! When using the script for the first time, a browser page will open with a Google request to link the account with the script. This will also create a `mycreds.txt` file containing this permission, so that subsequent runs of the script won't require to login each time.
5. The script then reads the `.csv` input and converts it into `geojson` format, while geocoding the address into *latitude* and *longitude* (see issue #12 for required improvements to the geocoding process).
6. The `json` file containing the list of skills (no duplicates) is created, as well as skill tags are assigned to each respective user in the final `geojson` file.

## TODO
The map still has *a lot* of things to fix. Below is a list of things to do and/or to fix; if an issue or a problem has already been created, then the title is clickable.

### [~~Fix the sidebar-content~~](#3)

### [Make the "social links" clickable](#1)

### [Add tag-filter for skills](#4) **update** the `index_OLD.html` now contains a *non-fully-working* prototype of the filter. Read comments in #4.

### [~~Add profile pictures~~](#5) **CLOSED**, but see #10

### [Simplify/lighten the map](#6)

### [Fully translate the map to English](#7)

### [~~Make the make responsive~~](#8) **CLOSED**, but see #11

### ~~Create a script to automatically convert the GoogleForm spreadsheet with answers to the required `.geojson` format~~ ** **CREATED**

### [Improve the "Google Form to geojson" script](#14)

### [Improve the Google Form to "force" a standard for certain answers](#12)

### Create social profiles for the map ?
Shall social profiles (twitter, facebook, etc...) be created for the map, or shall it just be included on CW website(s)?
