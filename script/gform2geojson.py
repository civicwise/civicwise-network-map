# -*- coding: utf-8 -*-
import csv
import re
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import geocoder
import os
import time
import itertools
import pygeoj
import json
from PIL import Image
from shutil import copyfile

'''
The script assumes that the .csv file (derived from the Google Form spreadsheet) is structured as follows:
column A = "Informazioni cronologiche" (date and time of when the questionnaire was filled in)
B = "Your name"
C = "Your surname"
D = "Where are you from (country)"
E = "Where are you based (address)"
F = "Describe yourself in one tweet"
G = "Your skille and competences"
H = "Language skills"
I = "Social links"
J = "Profile picture"
'''

######################################
#   MANUAL CONFIGURATION
######################################

#Change the input filename so that it matches the .csv file of the spreadsheet
gform_file = ('wisersmap_small.csv')

#Change the name of the geojson output (containing the map data)
geojson_name = ('test.geojson')

#Change the name of the json file (containing the categories for filtering the map points)
json_name = ('test.json')

#Copy your Google API Key in a file named google_api.txt and save it in the same folder as this script (see included example).
g_api_key_file = open("google_api.txt")


#######################################
#   SETUP THE ENVIRONMENT
#######################################

#Sets up the api key for the geocoder, if using Google Maps/Places.
read_api = g_api_key_file.readlines()
g_api_key = read_api[1]
os.environ["GOOGLE_API_KEY"] = str(g_api_key)

#Connect to Google Drive; the code for storing the credentials (and not having to re-login every time the script is run) is from
#https://stackoverflow.com/questions/24419188/automating-pydrive-verification-process/24542604#24542604

g_login = GoogleAuth()
drive = GoogleDrive(g_login)
g_login.LoadCredentialsFile("mycreds.txt")
if g_login.credentials is None:
    # Authenticate if they're not there
    g_login.LocalWebserverAuth()
elif g_login.access_token_expired:
    # Refresh them if expired
    g_login.Refresh()
else:
    # Initialize the saved creds
    g_login.Authorize()
# Save the current credentials to a file
g_login.SaveCredentialsFile("mycreds.txt")

#######################################
#   DATA COLLECTION
#######################################

#Check if the folder "images" already exists; if not, create it
if os.path.exists('images/'):
    pass
else:
    os.makedirs("images/")

#Start reading the csv file
with open(gform_file, 'r', newline='') as gform_file:
    file_output = pygeoj.new()
    readgform = csv.reader(gform_file, delimiter='\t', quotechar='|')
    prog_number = itertools.count()
    json_values = []

    # Skip the first row, containing the header
    next(readgform, None)
    rows = [r for r in readgform]
    for row in rows:
        gform_name = row[1].capitalize()
        gform_surname = row[2].capitalize()
        gform_country = row[3]
        gform_address = row[4]
        gform_bio = row[5]
        gform_skills = row[6]
        gform_language = row[7]
        gform_social = row[8]
        gform_pic_link = row[9]
        cartodb_id = next(prog_number)
        gform_user_skills = []
        print('Collecting details for *** ' + '\x1b[31;3m' + gform_name + ' ' + gform_surname + '\x1b[0m' + ' ***')

#Create a formatted list out of the gform_skills field
        for i in re.split(';|,', gform_skills):
            i = i.split()
            i = ' '.join(i).lower()
            i = re.sub('\.|\,|\;', '',i)
            gform_user_skills.append(i)
            print(i)
            json_values.append(i)

#Download the profile picture from the GDrive link
        gd_file_id = re.sub('https.*?id=','',str(gform_pic_link))
#Check if the GDrive ID exists; it's a primitive way of checking, and can surely be improved.
        if len(gform_pic_link) > 1:
            gd_file = drive.CreateFile({'id': str(gd_file_id)})
            print(gd_file['mimeType'])
#Check the file MIME
            if gd_file['mimeType'] == "image/png":
                png = gd_file.GetContentFile('tmpimg.png')
                pngopen = Image.open('tmpimg.png')
                #Convert the PNG to RGB, to avoid problems with RGBA format
                pngopen = pngopen.convert('RGB')
                pngopen.save('images/' + str(cartodb_id) + '.jpg',format = 'JPEG', quality = 75)
                os.remove('tmpimg.png')
            else:
#Here the file is downloaded, and is assigned a '.jpg' extension EVEN IF IT IS NOT A JPG IMAGE!
                gd_file_get = gd_file.GetContentFile('images/' + str(cartodb_id) + '.jpg')
#Wait 2 seconds before moving to the next action, to avoid API clogging
            time.sleep(2)
#If the user has no uploaded image, assign the general CW logo
        else:
            general_image = copyfile('cw_profile.jpg', 'images/' + str(cartodb_id) + '.jpg')

#Translate the address into lon and lat coordinates using OSM Nominatim API;
#Wait 3 seconds after each query, to avoid misusing the API
        req_address = geocoder.osm(str(gform_address))
        coord_address = req_address.latlng
        coord_lng = coord_address[0]
        coord_lat = coord_address[1]
        time.sleep(3)

#Create the geojson output structure
        file_output.add_feature(
            geometry={
                "type":"Point",
                "coordinates":[(str(coord_lat)),(str(coord_lng))]
                },
            properties={
            "cartodb_id":str(cartodb_id),
            "name":str(gform_name),
            "surname":str(gform_surname),
            "country":str(gform_country),
            "basis":str(gform_address),
            "lat":str(coord_lat),
            "lon":str(coord_lng),
            "shortbio":str(gform_bio),
            "skills":str(gform_skills),
            "skillstag":gform_user_skills,
            "languages":str(gform_language),
            "sociallinks":str(gform_social),
            "pic":"images/"+str(cartodb_id)+".jpg"
            })

#Delete duplicate values from the list of skills, and create a json key:values structure for the json file containing ALL the skills
    json_unique_values = list(set(json_values))
    json_keys = {"skillstag" : json_unique_values}

#Write the resulting structure to json
    with open(json_name, "w") as f:
        #print(json_unique_values)
#"ensure_ascii" preservers the accented letters; however it MAY cause issues with the map (as it is not UTF8)
        json_file = json.dump(json_keys, f, ensure_ascii=False)
        print(f)

    file_output.save(geojson_name)
